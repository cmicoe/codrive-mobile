import { Component, OnInit } from '@angular/core';
import { NavParams, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-user-action-alert',
  templateUrl: './user-action-alert.component.html',
  styleUrls: ['./user-action-alert.component.scss'],
})
export class UserActionAlertComponent implements OnInit {
  alertMessage: any
  constructor(public navParams: NavParams,
    private popoverController: PopoverController) { }

  ngOnInit() {

    console.log(this.navParams.data.message)
    this.alertMessage = this.navParams.data.message
  }
  closeAlert(confirmationMessage) {
    if (confirmationMessage === 'cancel') {
      this.popoverController.dismiss('false')
    } else {
      this.popoverController.dismiss('true')
    }
  }

}
