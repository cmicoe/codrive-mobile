import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { AuthgaurdService } from '../services/authgaurd/authgaurd.service';

const routes: Routes = [
    {
        path: '',
        component: HomePage,
        canActivate: [AuthgaurdService],
        children: [
            {
                path: 'my-route',
                loadChildren: () => import('../my-route/my-route.module').then(m => m.MyRoutePageModule)
            },
            {
                path: 'new-route',
                loadChildren: () => import('../new-route/new-route.module').then(m => m.NewRoutePageModule)
            },
            {
                path: 'search',
                loadChildren: () => import('../search/search.module').then(m => m.SearchPageModule)
            },
            {
                path: '',
                redirectTo: '/home/my-route',
                pathMatch: 'full'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class HomePageRoutingModule { }
