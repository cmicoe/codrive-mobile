import { Component, OnInit, ViewChild } from '@angular/core';
import { IonTabs, PopoverController } from '@ionic/angular';
import { UtilsService } from '../utils/app-utils';
import { Router, RouterEvent } from '@angular/router';
import { AuthgaurdService } from '../services/authgaurd/authgaurd.service';
import { JwtService } from '../services/framework/jwt.service';
import { AppPreferences } from '@ionic-native/app-preferences/ngx';
import { RouteService } from '../services/route/route.service';
import { GenericRouteViewComponent } from '../generic-route-view/generic-route-view.component';
import { PushService } from '../services/push/push.service';
import { UserActionAlertComponent } from '../user-action-alert/user-action-alert.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  viewName = 'MY RIDE';
  subscription: any;
  constructor(public utilsService: UtilsService, private router: Router,
    public authgaurd: AuthgaurdService, public appPreferences: AppPreferences, public jwtService: JwtService,
    public routeService: RouteService, public pushService: PushService, public popoverController: PopoverController) {
  }

  ngOnInit() {
    this.setPointerPosition()
  }

  setPointerPosition() {
    this.subscription = this.routeService.getPointerPosition().subscribe(pointerPosition => {
      if (pointerPosition === 'home') {
        this.viewName = 'MY RIDE'
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  changeTab(selectedTab) {
    this.viewName = selectedTab;
  }

  logout() {
    this.showActionAlert("Arr you sure you want to logout?", UserActionAlertComponent)
  }

  confirmLogout() {
    var uuid = Object.assign({}, (this.pushService.deviceDetails.uuid != null || (this.pushService.deviceDetails.uuid !== undefined) ? this.pushService.deviceDetails.uuid : null))
    this.pushService.deRegisterPushToken(uuid).subscribe(
      res => {
        console.log(res)

      }, err => {
        const errorMessage = 'Something went wrong!. Please try again';
        this.utilsService.statusToster(errorMessage, 'danger');
      });
    this.jwtService.destroyToken();
    this.utilsService.clearFCMTokens()
    this.authgaurd.setLoggedIn(false)
    this.router.navigate([''])

  }

  async showActionAlert(actionMessage, UserActionAlertComponent) {
    let alertMessage = { "message": actionMessage }
    const popover = await this.popoverController.create({
      component: UserActionAlertComponent,
      componentProps: alertMessage,
      cssClass: 'alert-action-pop-over-style',
      translucent: true
    });
    popover.onDidDismiss()
      .then((result) => {
        console.log(result)
        if (result.data === 'true') {
          this.confirmLogout()
        } else {
        }
      });
    return await popover.present();
  }
}
