import { Injectable } from '@angular/core';
import { PopoverController, ToastController, ModalController, LoadingController } from '@ionic/angular';
import { GenericRouteViewComponent } from '../generic-route-view/generic-route-view.component';
import { PopoverService } from '../popover.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { AppPreferences } from '@ionic-native/app-preferences/ngx';


@Injectable()

export class UtilsService {
  loading: HTMLIonLoadingElement;
  constructor(public popoverController: PopoverController,
    public popoverService: PopoverService,
    private toastCtrl: ToastController, public router:Router,
    public modalController: ModalController,
    private loadingCtrl: LoadingController,
    private appPreferences: AppPreferences) {

  }

  async showPopOverWithDataAndComponent(data,component) {
    const popover = await this.popoverController.create({
      component: component,
      componentProps: data,
      translucent: true,
      cssClass: "pop-over-style",
      showBackdrop: true

    });
    popover.onDidDismiss().then(res => {
      this.popoverService.popover = false;
    })
    return await popover.present();
  }

  handleError(error: HttpErrorResponse,color:string) {
    if (error.status === 401 || error.status === 403) {
        localStorage.removeItem('token');
        // this.router.navigate(['/login']);
    } else if (error.status > 500) {
        this.statusToster("Please try again after some time", color);
    } else {
        this.statusToster("Please try again after some time", color);
    }
}

  async statusToster(res: any, color) {
    const toast = await this.toastCtrl.create({
      message: res,
      duration: 2000,
      position: 'bottom',
      color: color,
      cssClass: "toasterText"

    });
    toast.onDidDismiss().then((res) => {
      
      // this.router.navigate(['home'])
    });
    toast.present();
  }

  async presentLoading(msg) {
    this.loading = await this.loadingCtrl.create({
      message: msg,
    });
    return await this.loading.present();
  }
  async dismissLoanding(){
    this.loading.dismiss()
  }

  async getFCMToken(){
    let fcmToken;
  await  this.appPreferences.fetch('fcmTokens', 'fcmTokens').then(res=>{
      if(res !== undefined || res!== null){
        fcmToken= res;
      }else{
        fcmToken='';
      }
    }),(err)=>{
      fcmToken='';
    }
    return fcmToken
  }

  storeFCMToken(FCMToken,existingToken){
    let fcmTokens={
      'old':(existingToken && existingToken.new)?existingToken.new:'',
      'new':FCMToken
    }
    return this.appPreferences.store('fcmTokens','fcmTokens',fcmTokens)
  }
  clearFCMTokens() {
    this.appPreferences.remove('fcmTokens', 'fcmTokens').then((response: any) => {
      console.log('FCM tokens removed');
    }).catch((error: any) => {
      console.log('Error removing FCM tokens');
    });
   
  }

}
