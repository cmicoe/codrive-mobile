import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { GenericRouteViewComponent } from './generic-route-view/generic-route-view.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UtilsService } from './utils/app-utils';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from './services/framework/api.service';
import { JwtService } from './services/framework/jwt.service';
import { AuthgaurdService } from './services/authgaurd/authgaurd.service';
import { AppPreferences } from '@ionic-native/app-preferences/ngx';
import { PickUpLocationsComponentComponent } from './pick-up-locations-component/pick-up-locations-component.component';
import { PushService } from './services/push/push.service';
import { Push } from '@ionic-native/push/ngx';
import { Device } from '@ionic-native/device/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { UserActionAlertComponent } from './user-action-alert/user-action-alert.component';

@NgModule({
  declarations: [AppComponent,GenericRouteViewComponent,PickUpLocationsComponentComponent,UserActionAlertComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,ReactiveFormsModule,    
    // Geolocation,    
    // NativeGeocoder,
    HttpClientModule,
  ],
  providers: [
    UtilsService,
    StatusBar,
    SplashScreen,
    AuthgaurdService,
    AppPreferences,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    ApiService,
    JwtService,
    Push,
    PushService,
    Device,
    AppVersion
    

  ],
 
  bootstrap: [AppComponent],
})
export class AppModule { }
