import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MyRoutePage } from './my-route.page';

describe('MyRoutePage', () => {
  let component: MyRoutePage;
  let fixture: ComponentFixture<MyRoutePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyRoutePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MyRoutePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
