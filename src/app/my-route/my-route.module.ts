import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyRoutePageRoutingModule } from './my-route-routing.module';

import { MyRoutePage } from './my-route.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyRoutePageRoutingModule
  ],
  declarations: [MyRoutePage]
})
export class MyRoutePageModule {}
