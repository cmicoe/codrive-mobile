import { Component, OnInit, NgZone } from '@angular/core';
import { RidesService } from '../services/rides/rides.service';
import { UtilsService } from '../utils/app-utils';
import { LoadingController, PopoverController } from '@ionic/angular';
import { PickUpLocationsComponentComponent } from '../pick-up-locations-component/pick-up-locations-component.component';
import { RouteService } from '../services/route/route.service';
import { Subscription } from 'rxjs';
import { async } from '@angular/core/testing';
import { UserActionAlertComponent } from '../user-action-alert/user-action-alert.component';
import { JwtService } from '../services/framework/jwt.service';

@Component({
  selector: 'app-my-route',
  templateUrl: './my-route.page.html',
  styleUrls: ['./my-route.page.scss'],
})

export class MyRoutePage implements OnInit {
  segmentType: string;
  upcomingRides: any = [];
  completedRides: any = [];
  subscription: any;
  loading: HTMLIonLoadingElement;
  confirmation: boolean;
  rideId: any;;
  rideStatus: any;
  user:any;
  selectedRideToUpdate:any
  constructor(
    private rideService: RidesService,
    private loadingCtrl: LoadingController,
    public utilsService: UtilsService,
    public routeService: RouteService,
    public popoverController: PopoverController,
    public jwtService: JwtService,
    private zone:NgZone) { }

  ngOnInit() {
    this.segmentType = 'upcoming';
    this.user=this.jwtService.getUser()
    this.getUpcomingRides();
    this.subcribeToPushNotificationObserver()
  }
 
  ionViewWillEnter() {
    this.user=this.jwtService.getUser()
  }


  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
      if (this.segmentType === "upcoming") {
        this.getUpcomingRides();
      } else {
        this.getCompletedRides();
      }
    }, 2000);
  }

  segmentChanged(ev: any) {
    this.segmentType == ev.detail.value
    if (ev.detail.value === "upcoming") {
      this.getUpcomingRides();
    } else {
      this.getCompletedRides();
    }
  }
  subcribeToPushNotificationObserver() {
     this.routeService.getPushNotificationObserver().subscribe(pushNotificationObj => {
       let pushObj=[];
       pushObj=pushNotificationObj
      if (pushObj && this.segmentType == 'upcoming') {
        this.getUpcomingRidesWithoutLoading()
      }
    });
  }
  async getUpcomingRides() {
    await this.utilsService.presentLoading('Loading...')
    this.rideService.getUpcomingRides().subscribe(res => {
      this.utilsService.dismissLoanding()
      if (res.status === false) {
        let errorMessage = res.errorMessage;
        this.utilsService.statusToster(errorMessage, 'danger');
        return;
      } else {
        this.upcomingRides = res.data.content;
      }
    }, err => {
      const errorMessage = 'Something went wrong!. Please try again';
      this.utilsService.dismissLoanding()
      this.utilsService.statusToster(errorMessage, 'danger');
    });
  }


  getUpcomingRidesWithoutLoading() {
    this.rideService.getUpcomingRides().subscribe(res => {
      if (res.status === false) {
        let errorMessage = res.errorMessage;
        console.log("Printing Errror in refreshing the my rides")
        console.log(errorMessage)

        return;
      } else {
        this.upcomingRides = res.data.content;
        
      }
    }, err => {
      console.log("Printing Errror in refreshing the my rides")
      console.log(err)
    });
  }

  getCompletedRidesWithoutLoading() {
    this.rideService.getCompletedRides().subscribe(res => {
      if (res.status === false) {
        let errorMessage = res.errorMessage;
        this.utilsService.statusToster(errorMessage, 'danger');
        return;
      } else {
        this.completedRides = res.data.content;
      }
    }, err => {
      const errorMessage = 'Something went wrong!. Please try again';
      this.utilsService.statusToster(errorMessage, 'danger');
    });
  }

  async getCompletedRides() {
    await this.utilsService.presentLoading('Loading...')
    this.rideService.getCompletedRides().subscribe(res => {
      this.utilsService.dismissLoanding()
      if (res.status === false) {
        let errorMessage = res.errorMessage;
        this.utilsService.statusToster(errorMessage, 'danger');
        return;
      } else {
        this.completedRides = res.data.content;
      }
    }, err => {
      const errorMessage = 'Something went wrong!. Please try again';
      this.utilsService.dismissLoanding()
      this.utilsService.statusToster(errorMessage, 'danger');
    });
  }

  updateRide(ride, rideStatus) {
    this.selectedRideToUpdate = ride
    this.rideId = ride.id;
    this.rideStatus = rideStatus
    
    if (rideStatus === "COMPLETED") {
      this.showActionAlert("Are you sure you want to complete the ride?", UserActionAlertComponent)
    }
    if (rideStatus === "CANCELLED") {
      this.showActionAlert("Are you sure you want to cancel the ride?", UserActionAlertComponent)
    }

  }

  requestCancelRideAsRidePartner(){
    this.rideService.cancelRide(this.rideId, this.rideStatus).subscribe(res => {
      this.utilsService.dismissLoanding()
      if (res.status === false) {
        let errorMessage = res.errorMessage;
        this.utilsService.statusToster(errorMessage, 'danger');
        return;
      } else {
        if (this.rideStatus === "COMPLETED") {
          this.utilsService.statusToster("Ride Completed", 'success');
        } else {
          this.utilsService.statusToster("Ride Cancelled", 'success');
        }
        this.getUpcomingRides();
      }
    }, err => {
      this.utilsService.dismissLoanding()
      if (err.status === 403) {
        this.utilsService.statusToster("Ride partner cannot cancel the ride", 'danger');
      } else {
        const errorMessage="Something went wrong!.Please try again"
        this.utilsService.statusToster(errorMessage, 'danger');
      }

    });
  }

  confirmUpdateRide() {
    var currentLoggedInUser = this.jwtService.getUser()
    var rideOwner = this.selectedRideToUpdate.route.rider.email
    if(this.rideStatus === "CANCELLED" && rideOwner !== currentLoggedInUser){
      this.requestCancelRideAsRidePartner()
      return
    }
    this.rideService.updateRideStatus(this.rideId, this.rideStatus).subscribe(res => {
      this.utilsService.dismissLoanding()
      if (res.status === false) {
        let errorMessage = res.errorMessage;
        this.utilsService.statusToster(errorMessage, 'danger');
        return;
      } else {
        if (this.rideStatus === "COMPLETED") {
          this.utilsService.statusToster("Ride Completed", 'success');
        } else {
          this.utilsService.statusToster("Ride Cancelled", 'success');
        }
        this.getUpcomingRides();
      }
    }, err => {
      this.utilsService.dismissLoanding()
      if (err.status === 403) {
        this.utilsService.statusToster("Ride partner cannot cancel the ride", 'danger');
      } else {
        const errorMessage="Something went wrong!.Please try again"
        this.utilsService.statusToster(errorMessage, 'danger');
      }

    });
  }

  showPickUpLocations(ride) {
    const data = ride;
    this.utilsService.showPopOverWithDataAndComponent(data, PickUpLocationsComponentComponent);
  }
  async showActionAlert(actionMessage, UserActionAlertComponent) {
    let alertMessage = { "message": actionMessage }
    const popover = await this.popoverController.create({
      component: UserActionAlertComponent,
      componentProps: alertMessage,
      cssClass: 'alert-action-pop-over-style',
      translucent: true
    });
    popover.onDidDismiss()
      .then((result) => {
        console.log(result)
        if (result.data==='true') {
          this.confirmUpdateRide()
        } else {

        }

      });
    return await popover.present();
  }
}



