import { Component, OnInit, NgZone, ElementRef } from '@angular/core';
import { FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { UtilsService } from '../utils/app-utils';
import { RouteService } from '../services/route/route.service';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';

declare var google;
const weekDaysListMaster = [
    { name: 'Su', isChecked: false, isDisabled: true, day: 1 },
    { name: 'M', isChecked: false, isDisabled: true, day: 2 },
    { name: 'T', isChecked: false, isDisabled: true, day: 3 },
    { name: 'W', isChecked: false, isDisabled: true, day: 4 },
    { name: 'Th', isChecked: false, isDisabled: true, day: 5 },
    { name: 'F', isChecked: false, isDisabled: true, day: 6 },
    { name: 'S', isChecked: false, isDisabled: true, day: 7 },
];

@Component({
    selector: 'app-new-route',
    templateUrl: './new-route.page.html',
    styleUrls: ['./new-route.page.scss'],
})
export class NewRoutePage implements OnInit {
    autocompleteItems: any[];
    placeTypeFocused: string;
    route = {};
    currentTime: any;
    hrs: any;
    min: any;
    seatCount = 1;
    public routeForm;
    listOfRoutes = [];
    pickupLocationDetails = [];
    dropLocationDetails = [];
    pickupPointDetails = [];
    selectedDayArray = [];
    weekDaysList = JSON.parse(JSON.stringify(weekDaysListMaster));
    GoogleAutocomplete: any;
    geocoder: any;
    isValidDate: any;
    sdate: string;
    edate: string;
    constructor(
        private fb: FormBuilder,
        public zone: NgZone,
        public utilsService: UtilsService,
        public routeService: RouteService,
        public router: Router,
        private elementRef: ElementRef
    ) {
        this.getCurrentTime();
        this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
        this.geocoder = new google.maps.Geocoder();
        this.autocompleteItems = [];
    }

    ngOnInit() {
        this.initialiseLoginForm();
        this.dateChanged();
    }

    initialiseLoginForm() {
        this.routeForm = this.fb.group({
            origin: ['', Validators.required],
            destination: ['', Validators.required],
            pickupPoint: [''],
            seatCount: [this.seatCount],
            time: [this.currentTime],
            WeekDay: [false],
            fromDate: ['', Validators.required],
            toDate: ['', Validators.required]
        });
        this.edate = '';
        this.sdate = '';
    }

    getCurrentTime() {
        const today = new Date();
        const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        this.hrs = today.getHours() > 10 ? today.getHours() : '0' + today.getHours();
        this.min = today.getMinutes() > 10 ? today.getMinutes() : '0' + today.getMinutes();
        this.currentTime = this.hrs + ':' + this.min;
        this.currentTime = "09:00"
    }

    addRoute() {
        if (this.routeForm.value.pickupPoint === '') {
            return;
        }
        else if (this.listOfRoutes.length >= 5) {
            this.utilsService.statusToster('Max five pickpoints is allowed.', 'danger');
            return;
        }
        this.listOfRoutes.push(this.routeForm.value.pickupPoint);
        this.routeForm.patchValue({ pickupPoint: '' });
    }
    deleteRoute(index) {
        this.listOfRoutes.splice(index, 1);
        this.pickupPointDetails.splice(index, 1);
    }
    addSeatCount() {
        if (this.routeForm.value.seatCount === 3) {
            return;
        }
        const setCount = ++this.routeForm.value.seatCount;
        this.routeForm.patchValue({ seatCount: setCount });
    }

    subSeatCount() {
        if (this.routeForm.value.seatCount === 1) {
            return;
        }
        const setCount = --this.routeForm.value.seatCount;
        this.routeForm.patchValue({ seatCount: setCount });

    }
    selectedDays(item, e, i) {
        if (e.detail.checked) {
            e.detail.value.day = i + 1;
            this.selectedDayArray.push(e.detail.value);
        } else {
            const index = this.selectedDayArray.findIndex(x => x.name == item.name);
            this.selectedDayArray.splice(index, 1);
        }
    }
    async submit() {
        await this.utilsService.presentLoading('Loading...');
        this.isValidDate = this.validateDates(this.routeForm.value.fromDate, this.routeForm.value.toDate);
        if (this.isValidDate) {
            if (this.selectedDayArray.length > 0) {
                const routeObj = this.createRouteObj();
                this.routeService.createRoute(routeObj).subscribe(res => {
                    this.utilsService.dismissLoanding();
                    if (res.status === false) {
                        const errorMessage = res.errorMessage;
                        this.utilsService.statusToster(errorMessage, 'danger');
                        return;
                    } else {
                        this.initialiseLoginForm();
                        this.listOfRoutes = [];
                        this.weekDaysList = JSON.parse(JSON.stringify(weekDaysListMaster));
                        this.selectedDayArray = [];
                        this.dateChanged();
                        const successMessage = 'Route Created Successfully';
                        this.routeService.updatePushNotificationObserver('myhome')
                        this.routeService.updatePointerPosition('home');
                        this.utilsService.statusToster(successMessage, 'success').then(res => {
                            this.router.navigate(['home']);
                        });
                    }
                }, (err) => {
                    this.utilsService.dismissLoanding();
                    const errorMessage = 'Something went wrong!. Please try again after some time';
                    this.utilsService.statusToster(errorMessage, 'danger');
                });
            } else {
                this.utilsService.dismissLoanding();
                this.utilsService.statusToster("please select a weekDay", 'danger')
            }
        }

    }

    createRouteObj() {
        let newRouteObj = {};
        const selectedDaysinWeekArray = [];
        this.selectedDayArray.forEach(item => {
            selectedDaysinWeekArray.push(item.day);
        });
        const selectedDaysinWeekString = selectedDaysinWeekArray.join(',');
        const startDateString = this.routeForm.value.fromDate.split('.');
        const endDateString = this.routeForm.value.toDate.split('.');
        return newRouteObj = {
            rider: null,
            origin: this.pickupLocationDetails[0].location,
            destination: this.dropLocationDetails[0].location,
            originLattitude: this.pickupLocationDetails[0].lat,
            originLongitude: this.pickupLocationDetails[0].lng,
            destinationLattitude: this.dropLocationDetails[0].lat,
            destinationLongitude: this.dropLocationDetails[0].lng,
            daysInWeek: selectedDaysinWeekString,
            startDate: startDateString[0],
            endDate: endDateString[0],
            startTime: this.routeForm.value.time,
            offeringSeats: this.routeForm.value.seatCount,
            pickupLocations: this.pickupPointDetails

        };
    }

    addFocus(placeTypeFocused: string) {
        console.log(placeTypeFocused)
        this.placeTypeFocused = placeTypeFocused;
    }

    clear() {
        this.autocompleteItems.length = 0;
    }

    validateDates(sDate: string, eDate: string) {
        this.isValidDate = true;
        if ((sDate != null && eDate != null) && (eDate) < (sDate)) {
            this.utilsService.statusToster('To date should be grater then from date', 'danger');
            this.isValidDate = false;
        }
        return this.isValidDate;
    }

    dateChanged() {
        this.routeForm.get('fromDate').valueChanges.subscribe(
            sdate => {
                console.log('sdate changed')
                this.sdate = sdate;
                if (this.edate === '') {
                    this.selectedDayArray = []
                    return
                }
                this.isValidDate = true;
                if ((this.sdate != null && this.edate != null) && (this.edate) < (this.sdate)) {
                    this.utilsService.statusToster('To date should be grater then from date', 'danger');
                    return;
                }
                this.validateDaysInDates(this.sdate, this.edate);
            }
        );

        this.routeForm.get('toDate').valueChanges.subscribe(
            edate => {
                console.log('edate changed')
                this.selectedDayArray = [];
                this.edate = edate;
                if (this.sdate === '') {
                    return
                }
                this.isValidDate = true;
                if ((this.sdate != null && this.edate != null) && (this.edate) < (this.sdate)) {
                    this.utilsService.statusToster('To date should be grater then from date', 'danger');
                    return;
                }
                this.validateDaysInDates(this.sdate, this.edate);
            }
        );
    }

    validateDaysInDates(sDate: string, eDate: string) {
        const startDate = new Date(sDate);
        const endDate = new Date(eDate);
        let count = 0;
        const newWeekDayList = JSON.parse(JSON.stringify(weekDaysListMaster));
        for (const i = startDate; i <= endDate;) {
            if (count > 7) {
                break;
            }
            count++;

            if (i.getDay() == 0) {
                i.setTime(i.getTime() + 1000 * 60 * 60 * 24);
                continue;
            }
            newWeekDayList[i.getDay()].isDisabled = false;
            newWeekDayList[i.getDay()].isChecked = true;
            newWeekDayList[i.getDay()].day= i.getDay()+1;
            i.setTime(i.getTime() + 1000 * 60 * 60 * 24);
        }
        this.weekDaysList = newWeekDayList;
        this.selectedDayArray = [];
        this.weekDaysList.forEach((element)=> {
            if (element.isChecked) {
                this.selectedDayArray.push(element)
            } 
        });
        console.log('pringtin week days array')
        console.log(this.selectedDayArray)
    }

    UpdateSearchResults() {
        const searchStr = this.routeForm.get(this.placeTypeFocused).value;
        if (searchStr === '') {
            this.autocompleteItems = [];
            return;
        }
        this.GoogleAutocomplete.getPlacePredictions({ input: searchStr, componentRestrictions: { country: 'in' } },
            (predictions: any[], status: any) => {
                this.autocompleteItems = [];
                if (predictions != null) {
                    const result = _.filter(predictions, p => {
                        return p.description.toLowerCase().indexOf('hyderabad') > -1
                            || p.description.toLowerCase().indexOf('secunderabad') > -1;
                    });
                    this.zone.run(() => {
                        result.forEach((prediction: any) => {
                            this.autocompleteItems.push(prediction);
                        });
                    });
                }
            });
    }

    SelectSearchResult(item) {
        const description = item.description;
        console.log(description)
        switch (this.placeTypeFocused) {
            case 'origin':
                this.routeForm.patchValue({ origin: description });
                break;
            case 'destination':
                this.routeForm.patchValue({ destination: description });
                break;
            case 'pickupPoint':
                this.routeForm.patchValue({ pickupPoint: description });
                break;
            default:
                break;
        }
        this.autocompleteItems.length = 0;
        this.geocoder.geocode({ placeId: item.place_id }, (results, status) => {
            const lat = results[0].geometry.location.lat();
            const lng = results[0].geometry.location.lng();
            if (this.placeTypeFocused === 'origin') {
                if (this.pickupLocationDetails.length == 1) {
                    this.pickupLocationDetails.shift();
                }
                this.pickupLocationDetails.push({ location: this.routeForm.value.origin, lat, lng });
            } else if (this.placeTypeFocused === 'destination') {
                if (this.dropLocationDetails.length == 1) {
                    this.dropLocationDetails.shift();
                }
                this.dropLocationDetails.push({ location: this.routeForm.value.destination, lat, lng });
            } else if (this.placeTypeFocused === 'pickupPoint') {
                if (this.pickupPointDetails.length === 5) {
                    return;
                }
                this.pickupPointDetails.push({ location: this.routeForm.value.pickupPoint, lattitude: lat, longitude: lng });
            }

        });
    }
    
    ionViewDidEnter() {
        this.dateChanged()
    }

    ionViewWillLeave() {
        this.initialiseLoginForm()
        this.selectedDayArray = [];
        this.weekDaysList = JSON.parse(JSON.stringify(weekDaysListMaster));
        this.sdate = ""
        this.edate = ""
        this.dateChanged()
    }

}
