import { Injectable } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { UtilsService } from 'src/app/utils/app-utils';
import { AppPreferences } from '@ionic-native/app-preferences/ngx';
import { Device } from '@ionic-native/device/ngx';
import { PushOptions, PushObject, Push } from '@ionic-native/push/ngx';
import { ApiService } from '../framework/api.service';
import { API_REGISTER_FOR_PUSH,API_REFRESHTOKEN_FOR_PUSH,API_DEREGISTER_FOR_PUSH,API_REGISTEREDDEVICES_FOR_PUSH } from 'src/environments/environment';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { RouteService } from '../route/route.service';

// declare var device: any;
const options: PushOptions = {
  android: {},
  ios: {
    alert: 'true',
    badge: true,
    sound: 'true'
  },
  browser: {
    pushServiceURL: 'http://push.api.phonegap.com/v1/push'
  }
};

@Injectable({
  providedIn: 'root'
})
export class PushService {
  deviceType: string = '';
  pushReceiveSubscription: Subscription;
  pushData: any;
  pushObject: PushObject;
  versionNumber: any;
  version: any
  constructor(public utilsService: UtilsService,
    private appPreferences: AppPreferences,
    public deviceDetails: Device,
    public apiService: ApiService,
    private push: Push,
    private appVersion: AppVersion,
    public routeService: RouteService
  ) { }

  initiatePush() {
    let device = this.deviceDetails
    if (device == null || device === undefined) {
      return;
    }
    if (device.platform !== 'ios') {
      this.createPushChannelForAndroid();
    }

    this.intializePushObj();
  }

  createPushChannelForAndroid() {
    this.push.createChannel({
      id: 'ComakeIT-coDrive',
      description: 'coDrive',
      // The importance property goes from 1 = Lowest, 2 = Low, 3 = Normal, 4 = High and 5 = Highest.
      importance: 5
    }).then(() => console.log(''));
  }

  intializePushObj() {
    if (this.pushObject) {
      console.log('Push object is already present');
      return;
    }
    this.pushObject = this.push.init(options);
    this.registerForPush();
    this.seAppInfo()
    
  }
  seAppInfo() {
    this.appVersion.getVersionNumber().then((response: any) => {
      this.versionNumber = response;
      this.version = response;
      this.appVersion.getVersionCode().then((versionNumber) => {
        this.version = this.version + ' (' + versionNumber + ')';
      }) .catch((error: any) => {
        console.error('Error while getting the app version code');
      });
    })
      .catch((error: any) => {
        console.error('Error while getting the app version number');
      });
  }
  registerForPush() {
    this.pushReceiveSubscription = this.pushObject.on('notification').subscribe((notification: any) => {
      console.log('Received a notification');
      if (notification.additionalData['force-start'] === '1') {
        console.log('Auto reply enabled notification, handling instantly');
        this.handlePushNotification(notification);
      } else if (notification.additionalData.foreground) {
        console.log('************* handling Instantly as app is running');
        this.handlePushNotification(notification);
      } else if (!notification.additionalData.foreground && notification.additionalData.coldstart) {
        console.log('App launched from tap on notification when killed');
        console.log('************* handling Postponed');
        this.pushData = notification;
      } else if (!notification.additionalData.foreground && !notification.additionalData.coldstart) {
        console.log('App launched from tap on notification when running in background');
        this.handlePushNotification(notification);
      } else {
        console.log('************* push ignored');
      }
    }, err=>{
      console.log(err)
    });
    this.pushObject.on('registration').subscribe((registration: any) => {
      console.log(registration.registrationId)
      this.storeFCMTokenLocally(registration.registrationId)
    },err=>{
      console.log(err)
    });
    this.pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
  }


  storeFCMTokenLocally(FCMToken) {
    this.utilsService.getFCMToken().then((existingToken) => {
      this.utilsService.storeFCMToken(FCMToken, existingToken).then(res => {
        this.sendFCMTokenToserver()
      }).catch((error:any)=>{
        console.log(error)
      })
    }).catch((error:any)=>{
      console.log(error)
    })

  }
  sendFCMTokenToserver() {
    let token;
    this.utilsService.getFCMToken().then(fcmtokens => {
      if (fcmtokens) {
        if (fcmtokens.old === undefined || fcmtokens.old === '' || fcmtokens.old === null ) {
          token = fcmtokens.new;
          let pushObjectForRegister = this.createPushObject(token)
          this.sendTokenToServerWithPostBody(pushObjectForRegister);
        }
       else if (fcmtokens.old !== fcmtokens.new) {
          token = fcmtokens.new;
          let refreshPushObj = {
            "uuid": this.deviceDetails.uuid,
            "token": token// Firebase Token
          }
          this.refreshFCMToken(refreshPushObj)
        } else {
          console.log('Tokens are same');
        }
      }

    }, (err) => {
      console.log("no tokens available")
    })
  }


  createPushObject(token) {
    return {
      "token": token,  // Firebase token
      "appVersion": this.versionNumber,
      "model": this.deviceDetails.model,
      "platform": this.deviceDetails.platform,
      "uuid": this.deviceDetails.uuid,
      "manufacturer": this.deviceDetails.manufacturer,
      "osVersion": this.deviceDetails.version,
      "isVirtual": false

    }
  }
  handlePushNotification(notification: any) {
    alert(notification.message)
    this.routeService.updatePushNotificationObserver(notification.message)

  }
  registeredDevices(){
    this.apiService.get(API_REGISTEREDDEVICES_FOR_PUSH).subscribe(response => {
      console.log(response)
    },err=>{
      console.log(err)
    })
  }
  sendTokenToServerWithPostBody(pushObjectForRegister) {
    this.apiService.post(API_REGISTER_FOR_PUSH, pushObjectForRegister).subscribe(response => {
      console.log(response)
      this.registeredDevices()
    },err=>{
      console.log(err)
    })
  }
  refreshFCMToken(refreshPushObj){
    this.apiService.put(API_REFRESHTOKEN_FOR_PUSH, refreshPushObj).subscribe(response => {
      console.log(response)
    },err=>{
      console.log(err)
    })
  }
  deRegisterPushToken(uuid) {
    if(this.deviceDetails === null || this.deviceDetails === undefined || this.deviceDetails.uuid === null
      || this.deviceDetails.uuid === undefined){
      //return null
    }
    let deResiterPushPbj = { "uuid": this.deviceDetails.uuid }
    this.clearPushData()
     return this.apiService.delete(API_DEREGISTER_FOR_PUSH, deResiterPushPbj)
  }
  clearPushData(){
    this.pushData=null;
    this.pushObject=null;
    this.pushReceiveSubscription.unsubscribe()
  }
}
