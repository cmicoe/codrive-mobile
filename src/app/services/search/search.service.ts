import { Injectable } from '@angular/core';
import { ApiService } from '../framework/api.service';
import { API_SEARCH_RIDE } from 'src/environments/environment';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

searchSubject = new Subject<any>();

  constructor(public apiService:ApiService ) { }

  searchRide(searchObject){
    let body = searchObject;
    return this.apiService.post(API_SEARCH_RIDE+"page=1&size=10",body)
  }

  updateSearchResulte(searchResult){
    this.searchSubject.next(searchResult)
  }
  getSearchResults(): Observable<any> {
    return this.searchSubject.asObservable();
}
}
