import { Injectable } from '@angular/core';
import { ApiService } from '../framework/api.service';
import { API_GET_UPCOMING_RIDE, API_GET_COMPLETED_RIDE, API_UPDATE_RIDE, API_CANCEL_RIDE_RIDE_PARTNER} from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class RidesService {

  constructor(public apiService: ApiService) { }

  getUpcomingRides() {
    return this.apiService.get(API_GET_UPCOMING_RIDE);
  }

  getCompletedRides() {
    return this.apiService.get(API_GET_COMPLETED_RIDE);
  }

  updateRideStatus(rideId,rideStatus){
    let path = API_UPDATE_RIDE + rideId + "?status=" + rideStatus;
    return this.apiService.put(path);
  }

  cancelRide(rideId,rideStatus){
    let path = API_CANCEL_RIDE_RIDE_PARTNER+ rideId ;
    return this.apiService.post(path);
  }

}
