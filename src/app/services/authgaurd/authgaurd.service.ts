import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthgaurdService {
  isLoggedIn:false;
  constructor(public router:Router) { }
  setLoggedIn(_value) {
    this.isLoggedIn = _value;
  }

  isAuthenticated(): boolean {
    return this.isLoggedIn;
  }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    console.log(this.isAuthenticated())
    if (!this.isAuthenticated()) {
      
      this.router.navigate(['']);
      return false;
    }
    return true;
  }
}
