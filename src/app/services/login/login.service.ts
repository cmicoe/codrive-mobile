import { Injectable } from '@angular/core';
import { ApiService } from '../framework/api.service';
import { API_LOGIN } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(public apiService:ApiService ) { }

  login(username,password){
    let body={"username":username,"password":password}
    return this.apiService.authPost(API_LOGIN,body)
  }
}
