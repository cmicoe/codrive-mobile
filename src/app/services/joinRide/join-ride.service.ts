import { Injectable } from '@angular/core';
import { ApiService } from '../framework/api.service';
import { API_JOIN_RIDE } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class JoinRideService {

  constructor(public apiService: ApiService) { }
  joinRide(rideId,pickuploacionId,dropLocationId) {
    let body = {
      "ride": {
        "id": rideId
      },
      "pickupLocation":{"id":pickuploacionId},
      "dropLocation":{"id":dropLocationId},
      "partner": null,
      "isRider": false

    }
    return this.apiService.post(API_JOIN_RIDE, body)
  }
}
