import { TestBed } from '@angular/core/testing';

import { JoinRideService } from './join-ride.service';

describe('JoinRideService', () => {
  let service: JoinRideService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JoinRideService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
