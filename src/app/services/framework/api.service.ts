import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { JwtService } from './jwt.service';
// import { API_ENDPOINT } from 'src/environments/environment';

@Injectable()
export class ApiService {
  constructor(
    private http: HttpClient,
    private jwtService: JwtService
  ) { }
  private setHeaders(): any {
    const headersConfig = {};
    if (this.jwtService.getToken()) {
      const token = this.jwtService
        .getToken()
        .replace('"', '')
        .replace('"', '');
      headersConfig['Authorization'] = token;
    }
    return headersConfig
  }

  get(
    path: string,
    params: URLSearchParams = new URLSearchParams()
  ): Observable<any> {
    return this.http
      .get(path, { headers: this.setHeaders() })
      .pipe(catchError(this.handleError));
  }

  put(url: string, body: Object = {}): Observable<any> {
    return this.http
      .put(url, body, { headers: this.setHeaders() })
      .pipe(catchError(this.handleError));
  }

  authPost(path: string, body): Observable<any> {
    return this.http
      .post(path, body)
      .pipe(
        catchError(this.handleError)
      );
  }

  post(url: string, body: Object = {}): Observable<any> {
    return this.http
      .post(url, body, { headers: this.setHeaders() })
      .pipe(catchError(this.handleError));
  }

  delete(url: string,body: Object = {}): Observable<any> {
    const token = this.jwtService.getToken().replace('"', '').replace('"', '');
    const options = {
      headers:  new HttpHeaders({
        'Authorization': token
      }),
      body:body
    }
    return this.http
      .delete(url,options)
      .pipe(catchError(this.handleError));
  }



  private handleError(error: HttpErrorResponse) {
    // this.appUtils.dismissLoading();
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      // this.appUtil.dismissLoading();
      console.log(
        `Backend returned code ${error.status}, ` + `body was: ${error.message}`
      );
    }
    // this.appUtil.dismissLoading();
    // return an observable with a user-facing error message
    return throwError(error);
  }


}
