import { Injectable } from '@angular/core';
import { ApiService } from '../framework/api.service';
import { API_CREATE_ROUTE } from 'src/environments/environment';
import { Observable, Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class RouteService {

  pointerPosition = new Subject<any>();
  pushNotificationObserver=new Subject<any>();
constructor(public apiService:ApiService) { }

createRoute(routeObj){
  let body = routeObj;
  return this.apiService.post(API_CREATE_ROUTE,body)
}

updatePointerPosition(view){
  this.pointerPosition.next(view)
}
getPointerPosition(): Observable<any> {
  return this.pointerPosition.asObservable();
}
updatePushNotificationObserver(pushNotificationData){
  this.pushNotificationObserver.next(pushNotificationData)
}
getPushNotificationObserver(): Observable<any> {
  return this.pushNotificationObserver.asObservable();
}


}
