import { Component, OnInit } from '@angular/core';
import { NavParams, PopoverController } from '@ionic/angular';
import { Route } from '@angular/compiler/src/core';

@Component({
    selector: 'app-pick-up-locations-component',
    templateUrl: './pick-up-locations-component.component.html',
    styleUrls: ['./pick-up-locations-component.component.scss'],
})
export class PickUpLocationsComponentComponent implements OnInit {
    pickupLocations = [];
    ride: any;
    constructor(public navParams: NavParams,
        private popoverController: PopoverController) {
        console.log('printing nav params');
        console.log(this.navParams);
        this.ride = this.navParams.data;
        if (this.ride != null || this.ride != undefined) {
            var arryToSort = this.ride.route.pickupLocations;
            var sortedArray = arryToSort.sort(function(a,b){
                return a.sequenceNo >b.sequenceNo?1:a.sequenceNo <b.sequenceNo?-1:0
               })
            this.pickupLocations = sortedArray;
        }
    }

    ngOnInit() { }

    closePopup() {
        this.popoverController.dismiss().then(res => {
        });
    }

}
