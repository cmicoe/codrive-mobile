import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../services/login/login.service';
import { AuthgaurdService } from '../services/authgaurd/authgaurd.service';
import { JwtService } from '../services/framework/jwt.service';
import { AppPreferences } from '@ionic-native/app-preferences/ngx';
import { UtilsService } from '../utils/app-utils';
import { NavController } from '@ionic/angular';
import { PushService } from '../services/push/push.service';
@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    public loginForm: FormGroup;
    @ViewChild('loginFrm') loginFrm;
    constructor(
        private router: Router,
        public navCtrl: NavController,
        public loginService: LoginService,
        public authgaurd: AuthgaurdService,
        public jwtService: JwtService,
        public utilsService: UtilsService,
        public pushService: PushService
    ) {
        this.initMobileForm();
    }

    ngOnInit() {
    }

    initMobileForm(): void {
        this.loginForm = new FormGroup({
            username: new FormControl('', [Validators.required]),
            password: new FormControl('', [Validators.required]),
            isConsentChecked: new FormControl(false, [Validators.required])
        });
    }

    toggleConsent(concentValue: boolean) {
        this.loginForm.patchValue({
            isConsentChecked: concentValue
        });
    }

    onSubmit() {
        const username = this.loginForm.value.username.trim();
        const password = this.loginForm.value.password;

        if (this.loginForm.invalid) {
            const fields = (username === '' && password === '') ? 'username and password' :
                (username === '' ? 'username' : (password === '' ? 'password' : ''));
            this.utilsService.statusToster(`Please provide your ${fields}`, 'danger');
            return;
        }
        if (!this.loginForm.value.isConsentChecked) {
            this.utilsService.statusToster(`Please accept the privacy policy to proceed`, 'danger');
            return;
        }
        this.loginService.login(username, password).subscribe(res => {
            if (res.status === false) {
                const errorMessage = 'Login failed. Please enter valid credentials.';
                this.utilsService.statusToster(errorMessage, 'danger');
                return;
            } else {
                this.pushService.initiatePush();
                const token = res.data.token;
                const user = res.data.email
                this.jwtService.saveUser(user)
                this.jwtService.saveToken(JSON.stringify(token))
                this.authgaurd.setLoggedIn(true);
                this.navCtrl.navigateRoot('home');
            }
        }, (error) => {
            const errorMessage = 'Login Failed. Please try again';
            this.utilsService.statusToster(errorMessage, 'danger');
        });
    }
}
