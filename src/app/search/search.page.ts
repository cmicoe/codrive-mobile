import { Component, OnInit, ElementRef, NgZone } from '@angular/core';
import { UtilsService } from '../utils/app-utils';
import { PopoverService } from '../popover.service';
import { Subscription } from 'rxjs';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SearchService } from '../services/search/search.service';
import { JoinRideService } from '../services/joinRide/join-ride.service';
import { PopoverController } from '@ionic/angular';
import * as _ from 'lodash';
import { RouteService } from '../services/route/route.service';
import { UserActionAlertComponent } from '../user-action-alert/user-action-alert.component';


declare var google;

const weekDaysListMaster = [
    { name: 'Su', isChecked: false, isDisabled: true ,day:1},
    { name: 'M', isChecked: false, isDisabled: true ,day:2},
    { name: 'T', isChecked: false, isDisabled: true ,day:3},
    { name: 'W', isChecked: false, isDisabled: true ,day:4},
    { name: 'Th', isChecked: false, isDisabled: true,day:5 },
    { name: 'F', isChecked: false, isDisabled: true ,day:6},
    { name: 'S', isChecked: false, isDisabled: true ,day:7},
];

@Component({
    selector: 'app-search',
    templateUrl: './search.page.html',
    styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
    displaySearchForm = true
    popover = false;
    navigationEndEventSubscription: Subscription;
    searchResultsArray = [];
    autocompleteItems: any[];
    route = {};
    currentTime: any;
    hrs: any;
    min: any;
    seatCount = 0;
    public searchForm;
    rideId: any;
    selectedRideToJoin: any

    GoogleAutocomplete: any;
    geocoder: any;
    weekDaysList = JSON.parse(JSON.stringify(weekDaysListMaster));
    placeTypeFocused: string;
    pickupLocationDetails = [];
    dropLocationDetails = [];
    selectedDayArray = [];
    isValidDate: any;
    sdate: string;
    edate: string;

    constructor(
        private fb: FormBuilder,
        public zone: NgZone,
        public popoverService: PopoverService,
        public searchService: SearchService,
        public utilsService: UtilsService,
        public joinRideService: JoinRideService,
        public router: Router,
        public popoverController: PopoverController,
        public routeService: RouteService
    ) {
        this.autocompleteItems = [];
        this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
        this.geocoder = new google.maps.Geocoder();
        this.getCurrentTime();
    }


    ngOnInit() {
        this.initSearchForm()
        this.selectedDayArray = [];
        this.searchResultsArray = []
        this.weekDaysList = JSON.parse(JSON.stringify(weekDaysListMaster));
        this.dateChanged()
    }

    initSearchForm() {
        this.searchForm = this.fb.group({
            origin: ['', Validators.required],
            destination: ['', Validators.required],
            time: this.currentTime,
            fromDate: ['', Validators.required],
            toDate: ['', Validators.required]
        });
    }

    switchToSearchView() {
        console.log("Clicked on filters")
        this.displaySearchForm = true
    }

    selectedDays(item, e, i) {
        console.log(i)
        // const receivedItems = <FormArray>this.routeForm.controls.receivedItems;
        if (e.detail.checked) {
            this.selectedDayArray.push(item)
        } else {
            let index = this.selectedDayArray.findIndex(x => x.name == item.name);
            this.selectedDayArray.splice(index, 1);
        }
    }

    getCurrentTime() {
        const today = new Date();
        const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        this.hrs = today.getHours() > 10 ? today.getHours() : '0' + today.getHours();
        this.min = today.getMinutes() > 10 ? today.getMinutes() : '0' + today.getMinutes();
        this.currentTime = this.hrs + ':' + this.min;
        this.currentTime = "09:00"
    }

    async search() {
        this.isValidDate = this.validateDates(this.searchForm.value.fromDate, this.searchForm.value.toDate);
        if (this.isValidDate) {
            if (this.selectedDayArray.length > 0) {
                await this.utilsService.presentLoading('Loading...')
                let searchObj = this.createSearchObj()
                this.searchService.searchRide(searchObj).subscribe(res => {
                    this.utilsService.dismissLoanding()
                    if (res.status === false) {
                        let errorMessage = res.errorMessage;
                        this.utilsService.statusToster(errorMessage, 'danger')
                        return;
                    } else {
                        this.searchResultsArray = res.data.content;
                        if (this.searchResultsArray.length > 0) {
                            this.displaySearchForm = false
                            this.utilsService.statusToster("success", 'success')
                        } else {
                            this.utilsService.statusToster("No rides found. Please try again.", 'danger')
                        }
                    }
                }, (err) => {
                    this.utilsService.dismissLoanding()
                    let errorMessage = "Something went wrong!. Please try again"
                    this.utilsService.statusToster(errorMessage, 'danger')
                });
            } else {
                this.utilsService.statusToster("please select a weekDay", 'danger')
            }
        }
    }

    createSearchObj() {
        let searchObj = {};
        let selectedDaysinWeekArray = []
        this.selectedDayArray.forEach(item => {
            selectedDaysinWeekArray.push(item.day)
        })
        let selectedDaysinWeekString = selectedDaysinWeekArray.join(',')
        let startDateString = this.searchForm.value.fromDate.split('T')
        let endDateString = this.searchForm.value.toDate.split('T')
        var startDateStringValue = startDateString[0] + 'T' + this.searchForm.value.time
        var endDateStringValue = endDateString[0] + 'T23:59'
        return searchObj = {
            "startDate": startDateStringValue,
            "endDate": endDateStringValue,
            // "startTime": this.searchForm.value.time,
            // "endTime": "",
            "source": this.pickupLocationDetails[0].location,
            "destination": this.dropLocationDetails[0].location,
            "daysOfWeek": selectedDaysinWeekString
        }
    }

    addFocus(placeTypeFocused: string) {
        this.placeTypeFocused = placeTypeFocused;
    }

    clear() {
        this.autocompleteItems.length = 0;
        // this.setLocationDescription('');
    }

    UpdateSearchResults() {
        const searchStr = this.searchForm.get(this.placeTypeFocused).value;
        if (searchStr === '') {
            this.autocompleteItems.length = 0;
            return;
        }
        else if (searchStr.length < 2) {
            return;
        }
        this.GoogleAutocomplete.getPlacePredictions({ input: searchStr, componentRestrictions: { country: 'in' } },
            (predictions: any[], status: any) => {
                this.autocompleteItems = [];
                const result = _.filter(predictions, p => {
                    return p.description.toLowerCase().indexOf('hyderabad') > -1
                        || p.description.toLowerCase().indexOf('secunderabad') > -1;
                });
                if (predictions != null) {
                    this.zone.run(() => {
                        result.forEach((prediction: any) => {
                            this.autocompleteItems.push(prediction);
                        });
                    });
                }
            });
    }

    dateChanged() {
        this.searchForm.get('fromDate').valueChanges.subscribe(
            sdate => {
                console.log('sdate changed')
                this.sdate = sdate;
                if (this.edate === '') {
                    this.selectedDayArray = []
                    return
                }
                this.isValidDate = true;
                if ((this.sdate != null && this.edate != null) && (this.edate) < (this.sdate)) {
                    this.utilsService.statusToster('To date should be grater then from date', 'danger');
                    return;
                }
                this.validateDaysInDates(this.sdate, this.edate);
            }
        );

        this.searchForm.get('toDate').valueChanges.subscribe(
            edate => {
                console.log('edate changed')
                this.selectedDayArray = [];
                this.edate = edate;
                if (this.sdate === '') {
                    return
                }
                this.isValidDate = true;
                if ((this.sdate != null && this.edate != null) && (this.edate) < (this.sdate)) {
                    this.utilsService.statusToster('To date should be grater then from date', 'danger');
                    return;
                }
                this.validateDaysInDates(this.sdate, this.edate);
            }
        );
    }

    validateDaysInDates(sDate: string, eDate: string) {
        const startDate = new Date(sDate);
        const endDate = new Date(eDate);
        let count = 0;
        const newWeekDayList = JSON.parse(JSON.stringify(weekDaysListMaster));
        for (const i = startDate; i <= endDate;) {
            if (count > 7) {
                break;
            }
            count++;

            if (i.getDay() == 0) {
                i.setTime(i.getTime() + 1000 * 60 * 60 * 24);
                continue;
            }
            newWeekDayList[i.getDay()].isDisabled = false;
            newWeekDayList[i.getDay()].isChecked = true;
            newWeekDayList[i.getDay()].day= i.getDay();
            i.setTime(i.getTime() + 1000 * 60 * 60 * 24);
        }
        this.weekDaysList = newWeekDayList;
        this.selectedDayArray = [];
        this.weekDaysList.forEach((element)=> {
            if (element.isChecked) {
                this.selectedDayArray.push(element)
            } 
        });
        console.log('pringtin week days array')
        console.log(this.selectedDayArray)
    }

    SelectSearchResult(item) {
        const description = item.description;
        switch (this.placeTypeFocused) {
            case 'origin':
                this.searchForm.patchValue({ origin: description });
                break;
            case 'destination':
                this.searchForm.patchValue({ destination: description });
                break;
            default:
                break;
        }
        this.autocompleteItems.length = 0;
        this.geocoder.geocode({ placeId: item.place_id }, (results, status) => {
            const lat = results[0].geometry.location.lat();
            const lng = results[0].geometry.location.lng();
            if (this.placeTypeFocused === 'origin') {
                if (this.pickupLocationDetails.length == 1) {
                    this.pickupLocationDetails.shift()
                }
                this.pickupLocationDetails.push({ "location": this.searchForm.value.origin, "lat": lat, "lng": lng });
            } else if (this.placeTypeFocused === 'destination') {
                if (this.dropLocationDetails.length == 1) {
                    this.dropLocationDetails.shift()
                }
                this.dropLocationDetails.push({ "location": this.searchForm.value.destination, "lat": lat, "lng": lng });
            }
        });
    }

    validateDates(sDate: string, eDate: string) {
        this.isValidDate = true;
        if ((sDate != null && eDate != null) && (eDate) < (sDate)) {
            this.utilsService.statusToster('To date should be grater then from date', 'danger');
            this.isValidDate = false;
        }
        return this.isValidDate;
    }

    joinRide(ride) {
        this.selectedRideToJoin = ride
        this.rideId = ride.id;
        this.showActionAlert('Are you sure you want to join Ride', UserActionAlertComponent)
    }

    async  confirmJoinRide() {
        var getPickUpAndDropLocationIds = this.getIdsForPickUpAndDropLocations()
        await this.utilsService.presentLoading('Loading...')
        var pickUpId = getPickUpAndDropLocationIds.pickUpId
        var dropId = getPickUpAndDropLocationIds.dropId
        this.joinRideService.joinRide(this.rideId, pickUpId, dropId).subscribe(res => {
            this.utilsService.dismissLoanding()
            if (res.data === 'Success') {
                //this.utilsService.statusToster(res.data, 'success')
                this.zone.run(() => {
                    this.router.navigate(['home/my-route']);
                })

            } else {
                this.utilsService.statusToster(res.data, 'danger')
            }
        }, (err) => {
            this.utilsService.dismissLoanding()
            let errorMessage = "Something went wrong!. Please try again"
            this.utilsService.statusToster(errorMessage, 'danger')
        });
    }

    getIdsForPickUpAndDropLocations() {
        var source = this.pickupLocationDetails[0].location
        var destination = this.dropLocationDetails[0].location
        var pickUpId = 0
        var dropId = 0
        var existingPickUpLocations = this.selectedRideToJoin.route.pickupLocations
        if (existingPickUpLocations.length > 0) {
            existingPickUpLocations.forEach(element => {
                if (source.location === element.location) {
                    pickUpId = element.id
                }
                if (destination.location === element.location) {
                    dropId = element.id
                }
            });
        }
        if (pickUpId == 0) {
            pickUpId = existingPickUpLocations[0].id
        }
        if (dropId == 0) {
            dropId = existingPickUpLocations[1].id
        }
        return { pickUpId: pickUpId, dropId: dropId }
    }

    async showActionAlert(actionMessage, UserActionAlertComponent) {
        let alertMessage = { "message": actionMessage }
        const popover = await this.popoverController.create({
            component: UserActionAlertComponent,
            componentProps: alertMessage,
            cssClass: 'alert-action-pop-over-style',
            translucent: true
        });
        popover.onDidDismiss()
            .then((result) => {
                console.log(result)
                if (result.data === 'true') {
                    this.confirmJoinRide()
                } else {

                }

            });
        return await popover.present();
    }

    ionViewDidEnter() {
        
    }

    ionViewWillLeave() {
        this.initSearchForm()
        this.sdate = ""
        this.edate = ""
        this.selectedDayArray = [];
        this.searchResultsArray = []
        this.weekDaysList = JSON.parse(JSON.stringify(weekDaysListMaster));
        this.dateChanged()
        this.displaySearchForm = true
    }
}
