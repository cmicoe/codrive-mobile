import { Component } from '@angular/core';
import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { environment } from 'src/environments/environment';
import { JwtService } from './services/framework/jwt.service';
import { AuthgaurdService } from './services/authgaurd/authgaurd.service';
import { Router } from '@angular/router';
import { PushService } from './services/push/push.service';

declare var window;

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        public navCtrl: NavController,
        public jwtService: JwtService,
        public router: Router,
        public authgaurd: AuthgaurdService,
        public pushService: PushService

    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.statusBar.backgroundColorByHexString('#ffa12a');
            this.splashScreen.hide();
            this.initializeGoogleAPIScript();
            this.listenBackEvent();

            if (this.jwtService.getToken()) {
                this.pushService.initiatePush();
                this.authgaurd.setLoggedIn(true);
                console.log('initialize app authgaurdService');
                this.navCtrl.navigateRoot('home');
            } else {
                this.navCtrl.navigateRoot('');
            }
        });
    }

    initializeGoogleAPIScript() {
        const script = document.createElement('script');
        script.id = 'googleMaps';
        const apiKey = environment.googleApiKey;
        script.src = 'https://maps.googleapis.com/maps/api/js?libraries=places&key=' + apiKey;
        document.body.appendChild(script);
    }

    listenBackEvent() {
        document.addEventListener('backbutton', () => {
            console.log(this.router.url);
            if (this.router.url === '/' ||
                this.router.url === '/home/my-route' ||
                this.router.url === '/home/new-route' ||
                this.router.url === '/home/search' ||
                this.router.url === '/home/alerts' ||
                this.router.url === '/login') {
                if (confirm('Are you sure you want to exit the app')) {
                    window.navigator.app.exitApp();
                }
            }
            return;
        });
    }
}
