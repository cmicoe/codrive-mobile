import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavParams, PopoverController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { PopoverService } from '../popover.service';
import { SearchService } from '../services/search/search.service';
import { UtilsService } from '../utils/app-utils';
import * as _ from 'lodash';

declare var google;

@Component({
    selector: 'app-generic-route-view',
    templateUrl: './generic-route-view.component.html',
    styleUrls: ['./generic-route-view.component.scss'],
})
export class GenericRouteViewComponent implements OnInit {
    autocompleteItems: any[];
    route = {};
    currentTime: any;
    hrs: any;
    min: any;
    seatCount = 0;
    public searchForm;

    GoogleAutocomplete: any;
    geocoder: any;
    weekDaysList = [{ name: 'M', isChecked: 'false' }, { name: 'T', isChecked: 'false' }, { name: 'W', isChecked: 'false' },
    { name: 'T', isChecked: 'false' }, { name: 'F', isChecked: 'false' }, { name: 'S', isChecked: 'false' }];
    placeTypeFocused: string;
    pickupLocationDetails = [];
    dropLocationDetails = [];
    selectedDayArray = [];
    constructor(
        private fb: FormBuilder,
        public zone: NgZone,
        public popoverService: PopoverService,
        public searchService: SearchService,
        public utilsService: UtilsService,
        public router: Router,
        public popoverController: PopoverController
    ) {
        this.autocompleteItems = [];
        this.GoogleAutocomplete = new google.maps.places.AutocompleteService();
        this.geocoder = new google.maps.Geocoder();
        this.getCurrentTime();
    }

    ngOnInit() {
        this.initSearchForm();
    }

    initSearchForm() {
        this.searchForm = this.fb.group({
            pickupLocation: ['', Validators.required],
            dropLocation: ['', Validators.required],
            time: this.currentTime,
            fromDate: ['', Validators.required],
            toDate: ['', Validators.required]
        });
    }

    selectedDays(item, e, i) {
        // const receivedItems = <FormArray>this.routeForm.controls.receivedItems;
        if (e.detail.checked) {
            e.detail.value.day = i + 1;
            this.selectedDayArray.push(e.detail.value);
        } else {
            const index = this.selectedDayArray.findIndex(x => x.name == item.name);
            this.selectedDayArray.splice(index, 1);
        }
    }

    getCurrentTime() {
        const today = new Date();
        const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        this.hrs = today.getHours() > 10 ? today.getHours() : '0' + today.getHours();
        this.min = today.getMinutes() > 10 ? today.getMinutes() : '0' + today.getMinutes();
        this.currentTime = this.hrs + ':' + this.min;
    }

    async search() {
        if (this.selectedDayArray.length > 0) {
            await this.utilsService.presentLoading('Loading...');
            const searchObj = this.createSearchObj();

            this.searchService.searchRide(searchObj).subscribe(res => {
                this.utilsService.dismissLoanding();
                if (res.status === false) {
                    const errorMessage = res.errorMessage;
                    this.utilsService.statusToster(errorMessage, 'danger');
                    return;
                } else {
                    const searchResult = res.data;
                    this.initSearchForm();
                    this.weekDaysList = [{ name: 'M', isChecked: 'false' }, { name: 'T', isChecked: 'false' }, { name: 'W', isChecked: 'false' },
                    { name: 'T', isChecked: 'false' }, { name: 'F', isChecked: 'false' }, { name: 'S', isChecked: 'false' }];
                    this.selectedDayArray = [];
                    const successMessage = 'success';
                    this.utilsService.statusToster(successMessage, 'success');
                    this.popoverController.dismiss().then(res => {
                        this.searchService.updateSearchResulte(searchResult);
                    });
                }
            }, (err) => {
                this.utilsService.dismissLoanding();
                const error = 'Something went wrong!. Please try again';

            });
        } else {
            this.utilsService.statusToster('please select a weekDay', 'danger');
        }

    }

    createSearchObj() {
        let searchObj = {};
        const selectedDaysinWeekArray = [];
        this.selectedDayArray.forEach(item => {
            selectedDaysinWeekArray.push(item.day);
        });
        const selectedDaysinWeekString = selectedDaysinWeekArray.join(',');
        const startDateString = this.searchForm.value.fromDate.split('T');
        const endDateString = this.searchForm.value.toDate.split('T');
        const startDateStringValue = startDateString[0] + 'T' + this.searchForm.value.time;
        const endDateStringValue = endDateString[0] + 'T23:00';
        return searchObj = {
            startDate: startDateStringValue,
            endDate: endDateStringValue,
            // "startTime": this.searchForm.value.time,
            // "endTime": "",
            source: this.pickupLocationDetails[0].location,
            destination: this.dropLocationDetails[0].location,
            daysOfWeek: selectedDaysinWeekString
        };
    }

    addFocus(placeTypeFocused: string) {
        this.placeTypeFocused = placeTypeFocused;
    }

    clear() {
        this.autocompleteItems.length = 0;
        // this.setLocationDescription('');
    }

    UpdateSearchResults() {
        const searchStr = this.searchForm.get(this.placeTypeFocused).value;
        if (searchStr === '') {
            this.autocompleteItems.length = 0;
            return;
        }
        else if (searchStr.length < 2) {
            return;
        }
        this.GoogleAutocomplete.getPlacePredictions({ input: searchStr, componentRestrictions: { country: 'in' } },
            (predictions: any[], status: any) => {
                this.autocompleteItems = [];
                const result = _.filter(predictions, p => {
                    return p.description.toLowerCase().indexOf('hyderabad') > -1
                        || p.description.toLowerCase().indexOf('secunderabad') > -1;
                });
                if (predictions != null) {
                    this.zone.run(() => {
                        result.forEach((prediction: any) => {
                            this.autocompleteItems.push(prediction);
                        });
                    });
                }
            });
    }

    SelectSearchResult(item) {
        const description = item.description;
        switch (this.placeTypeFocused) {
            case 'pickupLocation':
                this.searchForm.patchValue({ pickupLocation: description });
                break;
            case 'dropLocation':
                this.searchForm.patchValue({ dropLocation: description });
                break;
            case 'pickupPoint':
                this.searchForm.patchValue({ pickupPoint: description });
                break;
            default:
                break;
        }
        this.autocompleteItems.length = 0;
        this.geocoder.geocode({ placeId: item.place_id }, (results, status) => {
            const lat = results[0].geometry.location.lat();
            const lng = results[0].geometry.location.lng();
            if (this.placeTypeFocused === 'pickupLocation') {
                if (this.pickupLocationDetails.length == 1) {
                    this.pickupLocationDetails.shift();
                }
                this.pickupLocationDetails.push({ location: this.searchForm.value.pickupLocation, lat, lng });
            } else if (this.placeTypeFocused === 'dropLocation') {
                if (this.dropLocationDetails.length == 1) {
                    this.dropLocationDetails.shift();
                }
                this.dropLocationDetails.push({ location: this.searchForm.value.dropLocation, lat, lng });
            }
        });
    }
}
